class ApplicationController < ActionController::Base
    protected

    def after_sign_in_path_for(resource)
      root_path # Redirects to the root path after sign-in
    end
end
