class CommentsController < ApplicationController
    
    def create
        @lesson = Lesson.find(params[:lesson_id])
        @comment = @lesson.comments.new(comment_params)
    
        if @comment.save
          redirect_to @lesson, notice: 'Comment was successfully posted.'
        else
          # Handle the error, e.g., render the lesson's show view again
        end
    end

    private
    
    def comment_params
        params.require(:comment).permit(:user_email, :rating, :comment_text, :image)
    end
end
