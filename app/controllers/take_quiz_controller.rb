class TakeQuizController < ApplicationController
    before_action :authenticate_user!, only: [:index, :show]

    def index
        @quizzes = Quiz.all
    end
    
    def show
        @quiz = Quiz.find(params[:id])
        @quiz_attempts = QuizAttempt.where(user: current_user, quiz: @quiz).order(created_at: :desc)
    end

    def submit
        quiz = Quiz.find(params[:quiz_id])
        score = calculate_score(quiz, params[:answers])
      
        # Create a new QuizAttempt record
        quiz_attempt = QuizAttempt.new(user: current_user, quiz: quiz, score: score)
      
        if quiz_attempt.save
          # The record was successfully saved
          # redirect_to some_path, notice: 'Quiz attempt was successfully recorded.'
          redirect_to take_quiz_path(quiz), notice: 'Quiz attempt was successfully recorded.'
        else
          # The record was not saved due to validation errors
          #redirect_to another_path, alert: 'There was an error recording your quiz attempt.'
          redirect_to take_quiz_path(quiz), notice: 'Quiz attempt was successfully recorded.'
        end
    end

    def calculate_score(quiz, answers)
        score = 0
      
        # Iterate through each question in the quiz
        quiz.questions.each do |question|
          # Find the submitted answer for this question
          submitted_answer = answers[question.id.to_s]
      
          # Check if the submitted answer is correct
          if submitted_answer.present? && question.correct_answer == submitted_answer
            score += 1  # Increment the score for correct answers
          end
        end
      
        score
    end

end
