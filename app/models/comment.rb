class Comment < ApplicationRecord
  belongs_to :lesson
  has_one_attached :image

  validates :rating, numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 5 }
end
