class Lesson < ApplicationRecord
    has_many :comments, dependent: :destroy
    has_one_attached :thumbnail
end
