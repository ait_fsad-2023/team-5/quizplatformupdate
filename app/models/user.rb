class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  after_initialize :set_default_role, if: :new_record?

  def set_default_role
    self.role ||= 'student'
  end

  def teacher?
    email == 'teacher@quizapp.com'
  end

  has_many :quiz_attempts
  
end
