Rails.application.routes.draw do
  resources :leaderboards
  devise_for :users
  resources :lessons
  resources :questions
  resources :quizzes
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check
  # post 'submit_quiz', to: 'take_quiz#submit'
  post 'take_quiz/:quiz_id/submit', to: 'take_quiz#submit', as: :submit_quiz

  resources :quizzes do
    resources :questions
  end

  resources :lessons do
    resources :comments, only: [:create]
  end

  resources :leaderboards, only: [:index]
  
  resources :take_quiz, only: [:index, :show]
  # Defines the root path route ("/")
  # root "posts#index"
  root to: "lessons#index"
end
