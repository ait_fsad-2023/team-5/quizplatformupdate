class CreateLessons < ActiveRecord::Migration[7.1]
  def change
    create_table :lessons do |t|
      t.string :title
      t.text :description
      t.string :video_url
      t.string :thumbnail

      t.timestamps
    end
  end
end
