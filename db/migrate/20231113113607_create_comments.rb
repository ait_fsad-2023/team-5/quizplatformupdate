class CreateComments < ActiveRecord::Migration[7.1]
  def change
    create_table :comments do |t|
      t.string :user_email
      t.integer :rating
      t.text :comment_text
      t.references :lesson, null: false, foreign_key: true

      t.timestamps
    end
  end
end
