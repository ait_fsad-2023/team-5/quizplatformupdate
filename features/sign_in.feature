Feature: User Authentication

  Scenario: User Sign Up
    Given I am on the sign up page
    When I enter new user credentials
    Then I should be redirected to the sign in page

  Scenario: Successful sign in
    Given a registered user exists
    And I am on the sign in page
    When I enter valid credentials
    Then I should see the lessons content

# Additional scenarios can be added here
