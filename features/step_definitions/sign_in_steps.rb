# Step definitions for User Sign Up
Given('I am on the sign up page') do
  visit new_user_registration_path # Update with your app's sign-up path
end

When('I enter new user credentials') do
  fill_in 'Email', with: 'student5@gmail.com'
  fill_in 'Password', with: '1q2w3e4r'
  fill_in 'Password confirmation', with: '1q2w3e4r'
  click_button 'Sign up'
end

Then('I should be redirected to the sign in page') do
  # put page.html
  # expect(page).to have_current_path(new_user_session_path)
  expect(page).to have_text('Lessons')
end

# Step definitions for User Sign In
Given('a registered user exists') do
  User.create!(email: 'student5@gmail.com', password: '1q2w3e4r', password_confirmation: '1q2w3e4r') # Replace with your user model and attributes
end

Given('I am on the sign in page') do
  visit new_user_session_path
end

When('I enter valid credentials') do
  fill_in 'Email', with: 'student5@gmail.com'
  fill_in 'Password', with: '1q2w3e4r'
  click_button 'Log in'
end

Then('I should see the lessons content') do
  expect(current_path).to eq('/') # Update with your index page path if different
  expect(page).to have_text('Lessons')
end
